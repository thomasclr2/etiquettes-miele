<?php

namespace EtiquetteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use EtiquetteBundle\Form\UserType;
use EtiquetteBundle\Entity\User;

class UserController extends Controller
{
    public function listAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$users = $em->getRepository(User::class)->findAll();
    	
    	return $this->render('@Etiquette/User/list.html.twig', array(
    		'users' => $users,
    		'css_file' => 'produits'
    	));
    }

    public function viewAction(User $user, Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('submit', SubmitType::class, array('label' => 'Supprimer'))
            ->getForm()
        ;

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            return $this->redirectToRoute('user_list');
        }

    	return $this->render('@Etiquette/User/view.html.twig', array(
    		'user' => $user,
            'form' => $form->createView(),
    	));
    }

    public function addAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	$user = new User();
    	$form = $this->createForm(UserType::class, $user);

    	$form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $passwordEncoder = $this->get('security.password_encoder');
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

        	$em->persist($user);
        	$em->flush();

        	return $this->redirectToRoute('user_list');
        }
    	
    	return $this->render('@Etiquette/User/add.html.twig', array(
    		'form' => $form->createView(),
    	));
    }

    public function editAction(Request $request, User $user)
    {
    	$em = $this->getDoctrine()->getManager();
    	$form = $this->createForm(UserType::class, $user);

    	$form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $passwordEncoder = $this->get('security.password_encoder');
            $newEncodedPassword = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($newEncodedPassword);

        	$em->persist($user);
        	$em->flush();

        	return $this->redirectToRoute('user_list');
        }
    	
    	return $this->render('@Etiquette/User/edit.html.twig', array(
    		'form' => $form->createView(),
    	));
    }
}