<?php

namespace EtiquetteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use EtiquetteBundle\Entity\Produit;

class MainController extends Controller
{
    public function indexAction()
    {
        //return $this->render('EtiquetteBundle:Default:index.html.twig');
        return $this->render('@Etiquette/Main/index.html.twig', array('css_file' => "dashboard"));
    }

    public function aideAction()
    {
      if (!$this->get('security.authorization_checker')->isGranted('ROLE_INTERNE')) {
        // Sinon on déclenche une exception « Accès interdit »
        return $this->redirectToRoute('dashboard');
        throw new AccessDeniedException('Accès limité aux interne.');
      }
        return $this->render('@Etiquette/Main/aide.html.twig');
    }
}
