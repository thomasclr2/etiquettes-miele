<?php

namespace EtiquetteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use EtiquetteBundle\Entity\Famille;

class FamilleController extends Controller
{
    public function famillesAction(){
      $em = $this->getDoctrine()->getManager();
      $familles = $em->getRepository('EtiquetteBundle:Famille')->findAll();
      return $this->render("@Etiquette/Famille/list.html.twig", array('css_file' => "familles", 'familles' => $familles));
    }

    public function addAction(Request $request){

      if ($request->isMethod('POST')) {

        $famille = new Famille();
        $famille->setNom($request->request->get('nom'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($famille);
        $em->flush();

        $request->getSession()->getFlashBag()->add('notice', 'La famille a bien été enregistrée.');

        // Puis on redirige vers la page de visualisation de cettte annonce
        return $this->redirectToRoute('famille_list');
      }

      return $this->render('@Etiquette/Famille/add.html.twig');
    }

    public function editAction($id, Request $request){

      if ($request->isMethod('POST')) {
        $famille = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Famille')->find($id);
        $famille->setNom($request->request->get('nom'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($famille);
        $em->flush();
        $request->getSession()->getFlashBag()->add('notice', 'La famille a bien été modifiée.');

        // Puis on redirige vers la page de visualisation
        return $this->redirectToRoute('famille_list', array('id' => $id));
      }

      $famille = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Famille')->find($id);
      return $this->render('@Etiquette/Famille/edit.html.twig', array('famille' => $famille));
    }
}
