<?php

namespace EtiquetteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use EtiquetteBundle\Entity\Produit;
use EtiquetteBundle\Entity\Famille;

class EtiquetteController extends Controller
{
    public function etiquettesAction(){
      $em = $this->getDoctrine()->getManager();
      $produits = $em->getRepository('EtiquetteBundle:Produit')->findAll();
      return $this->render("@Etiquette/Etiquette/list.html.twig", array('css_file' => "produits", 'produits' => $produits));
    }

    public function viewAction($id)
    {
      $produit = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Produit')->find($id);
      return $this->render("@Etiquette/Etiquette/view.html.twig", array('css_file' => "etiquettes", 'produit' => $produit));
    }

    public function printProduitAction($id, Request $request)
    {

      if ($request->isMethod('POST')) {
      $manager = $this->get('assets.packages');
      $produit = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Produit')->find($id);
      // Creation du document PDF - Format origine A4
      $pdf = $this->get("white_october.tcpdf")->create('PDF_PAGE_ORIENTATION', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // Desactive tous graphismes dans le header et le footer - barres etc.
      $pdf->SetPrintHeader(false);
      $pdf->SetPrintFooter(false);

      // ---------------------------------------------------------
      // Récupérer les valeurs du formulaire
      $reftarif = $produit->getReftarif();
      $famille = $produit->getFamille()->getNom();
      $codesap = $produit->getCodesap();
      $codeean = $produit->getCodeean();
      $modepose = $produit->getModepose();
      $ecopart = $produit->getEcopart();
      $pvcm = $produit->getPvcm();
      $hauteurnet = $produit->getHauteurnet();
      $largeurnet = $produit->getLargeurnet();
      $profondeurnet = $produit->getProfondeurnet();
      $existe = $produit->getExiste();
      $arg1 = $produit->getInfo1();
      $arg2 = $produit->getInfo2();
      $arg3 = $produit->getInfo3();
      $arg4 = $produit->getInfo4();
      $arg5 = $produit->getInfo5();
      //$photo = $produit->getPhoto();
      $prix = $request->request->get('prix');
      $prixTotal = floatval($prix) + floatval($ecopart);
      // New info for 2015 etiquettes
      $during = $produit->getDuring();
      $picto1 = "";
      $picto2 = "";
      $picto3 = "";
      $picto4 = "";
      $picto5 = "";
      $picto6 = "";
      // New info 2017
      $garantie = $produit->getGarantie();


      // Récupérer la date
      $date = date("Ymd");

      // ---------------------------------------------------------
      // Police origine
      $pdf->SetFont('helvetica', '', '');

      // add a page
      $pdf->AddPage();

      // MEMBER CHECK
      //$pdf->SetFont('helvetica', 'B', 12);
      //$pdf->SetTextColor(0, 0, 0);
      //$pdf->MultiCell(90, 6, '' . $member_id_stats, 0, 'L', 0, 0, 36, 15, true, 0); // Big

      // Information Taille
      $pdf->SetFont('helvetica', '', 8);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(180, 30, 'Format 2014 : 148x115mm', 0, 'L', 0, 0, 55, 18, true, 0);
      $pdf->MultiCell(180, 30, 'Format 2015 : 133x85mm', 0, 'L', 0, 0, 55, 154, true, 0);

      // ***********************************************************
      // Création et positionnement des Multicell
      // FAMILLE
      $pdf->SetFont('helvetica', 'B', 12);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(90, 6, '' . $famille, 0, 'L', 0, 0, 36, 28, true, 0); // Big
      $pdf->SetFont('helvetica', 'B', 10);
      $pdf->MultiCell(90, 6, '' . $famille, 0, 'L', 0, 0, 36, 163, true, 0); // Small

      // REF TARIF
      $pdf->SetFont('helvetica', '', 18);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(90, 8, '' . $reftarif, 0, 'L', 0, 0, 36, 33, true, 0); // Big
      $pdf->SetFont('helvetica', '', 16);
      $pdf->MultiCell(90, 8, '' . $reftarif, 0, 'L', 0, 0, 36, 168, true, 0); // Small

      // ARGUMENTS
      $pdf->SetFont('helvetica', '', 12); // Ancienne taille des arg 13 - modif le 02/04/15
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(140, 8, $arg1, 0, 'L', 0, 0, 35, 45, true, 0);  // Big
      $pdf->SetFont('helvetica', '', 11);
      $pdf->MultiCell(124, 6, $arg1, 0, 'L', 0, 0, 35, 179, true, 0); // Small

      $pdf->SetFont('helvetica', '', 12);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(140, 8, $arg2, 0, 'L', 0, 0, 35, 57, true, 0);  // Big
      $pdf->SetFont('helvetica', '', 11);
      $pdf->MultiCell(124, 6, $arg2, 0, 'L', 0, 0, 35, 186, true, 0); // Small

      $pdf->SetFont('helvetica', '', 12);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(140, 8, $arg3, 0, 'L', 0, 0, 35, 69, true, 0);  // Big
      $pdf->SetFont('helvetica', '', 11);
      $pdf->MultiCell(124, 6, $arg3, 0, 'L', 0, 0, 35, 193, true, 0); // Small

      $pdf->SetFont('helvetica', '', 12);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(140, 8, $arg4, 0, 'L', 0, 0, 35, 81, true, 0);  // Big
      $pdf->SetFont('helvetica', '', 11);
      $pdf->MultiCell(124, 6, $arg4, 0, 'L', 0, 0, 35, 200, true, 0); // Small

      $pdf->SetFont('helvetica', '', 12);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(140, 8, $arg5, 0, 'L', 0, 0, 35, 93, true, 0);  // Big
      $pdf->SetFont('helvetica', '', 11);
      $pdf->MultiCell(124, 6, $arg5, 0, 'L', 0, 0, 35, 207, true, 0); // Small

      // DURING + GARANTIE + EXISTE
      $pdf->SetFont('helvetica', '', 10);
      $pdf->SetTextColor(0, 0, 0);
      // $pdf->MultiCell(140, 4, $during, 0, 'L', 0, 0, 35, 102, true, 0); // Big
      $pdf->MultiCell(140, 4, $during, 0, 'L', 0, 0, 37.5, 92, true, 0); // Big
      $pdf->SetFont('helvetica', '', 8);
      // $pdf->MultiCell(124, 4, $during, 0, 'L', 0, 0, 35, 213, true, 0); // Small
      $pdf->MultiCell(124, 4, $during, 0, 'L', 0, 0, 37.5, 205, true, 0); // Small 2017

      $pdf->SetFont('helvetica', '', 10);
      $pdf->MultiCell(140, 4, $garantie, 0, 'L', 0, 0, 37.5, 97, true, 0); // big 2017
      $pdf->SetFont('helvetica', '', 8);
      $pdf->MultiCell(124, 4, $garantie, 0, 'L', 0, 0, 37.5, 209, true, 0); // Small 2017

      $pdf->SetFont('helvetica', '', 10);
      $pdf->SetTextColor(0, 0, 0);
      // $pdf->MultiCell(140, 4, $existe, 0, 'L', 0, 0, 35, 107, true, 0); // Big
      $pdf->MultiCell(140, 4, $existe, 0, 'L', 0, 0, 37.5, 107, true, 0); // Big 2017
      $pdf->SetFont('helvetica', '', 8);
      // $pdf->MultiCell(124, 4, $existe, 0, 'L', 0, 0, 35, 217, true, 0); // Small
      $pdf->MultiCell(124, 4, $existe, 0, 'L', 0, 0, 37.5, 217, true, 0); // Small 2017

      // TAILLE
      $pdf->SetFont('helvetica', '', 8);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(78, 4, 'Dim. (H x L x P) : ' . $hauteurnet . ' x ' . $largeurnet . ' x ' . $profondeurnet . ' mm', 0, 'L', 0, 0, 35, 130, true, 0); // NEW
      $pdf->SetFont('helvetica', '', 7);
      // $pdf->MultiCell(78, 4, 'Dim. (H x L x P) : ' . $hauteurnet . ' x ' . $largeurnet . ' x ' . $profondeurnet . ' mm', 0, 'L', 0, 0, 35, 236, true, 0); // OLD
      $pdf->MultiCell(78, 4, 'Dim. (H x L x P) : ' . $hauteurnet . ' x ' . $largeurnet . ' x ' . $profondeurnet . ' mm', 0, 'L', 0, 0, 35, 228, true, 0); // 2017

      // CODE EAN + SAP
      $pdf->SetFont('helvetica', '', 8);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(110, 4, 'Code EAN : ' . $codeean . ' - Code SAP : ' . $codesap . ' - ' . $modepose . ' - 9' . $date . '9', 0, 'L', 0, 0, 35, 134, true, 0); // NEW
      $pdf->SetFont('helvetica', '', 7);
      // $pdf->MultiCell(88, 4, 'Code EAN : ' . $codeean . ' - Code SAP : ' . $codesap . ' - ' . $modepose . ' - 9' . $date . '9', 0, 'L', 0, 0, 35, 240, true, 0); // OL
      $pdf->MultiCell(88, 4, 'Code EAN : ' . $codeean . ' - Code SAP : ' . $codesap . ' - ' . $modepose . ' - 9' . $date . '9', 0, 'L', 0, 0, 35, 232, true, 0); // 2017


      // Price
      $pdf->SetFont('helvetica', '', 17);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(35, 7, '' . $prix . ' €', 0, 'R', 0, 0, 112, 119, true, 0); // Big
      $pdf->SetFont('helvetica', '', 16);
      // $pdf->MultiCell(35, 7, '' . $prix . ' €', 0, 'R', 0, 0, 99, 227, true, 0); // Small
      $pdf->MultiCell(35, 7, '' . $prix . ' €', 0, 'R', 0, 0, 99, 223, true, 0); // 2017

      $pdf->SetFont('helvetica', '', 10);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(38, 4, '+ ' . $ecopart . ' € d\'éco-part.' , 0, 'L', 0, 0, 147, 120, true, 0); // Big
      $pdf->SetFont('helvetica', '', 9);
      // $pdf->MultiCell(35, 4, '+ ' . $ecopart . ' € d\'éco-part.' , 0, 'L', 0, 0, 134, 228, true, 0); // Small
      $pdf->MultiCell(35, 4, '+ ' . $ecopart . ' € d\'éco-part.' , 0, 'L', 0, 0, 134, 224, true, 0); // 2017

      $pdf->SetFont('helvetica', '', 17);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(60, 4, 'soit ' . $prixTotal . ' €' , 0, 'R', 0, 0, 116, 126, true, 0); // Big
      $pdf->SetFont('helvetica', '', 16);
      // $pdf->MultiCell(40, 4, 'soit ' . $prixTotal . ' €' , 0, 'R', 0, 0, 120, 233, true, 0); // Small
      $pdf->MultiCell(40, 4, 'soit ' . $prixTotal . ' €' , 0, 'R', 0, 0, 120, 227, true, 0); // 2017


      // STYLE LINE
      //if ($member_id_stats == 1 OR $member_id_stats == 87 OR $member_id_stats == 86 OR $member_id_stats == 102 OR $member_id_stats == 118) {
      //$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
      //} else {
      //    $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(134, 26, 34));
      //}
      $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
      // RED LINE Etiquette Big
      $pdf->Line(33, 42, 177, 42, $style); // Creation line haute
      $pdf->Line(33, 113, 177, 113, $style); // Creation line bas
      // RED LINE Etiquette Small
      $pdf->Line(33, 177, 162, 177, $style); // Creation line haute
      $pdf->Line(33, 222, 162, 222, $style); // Creation line bas

      // Pictogrammes
      if (strlen($picto1)) {
      	$pdf->Image('../pictograms/'.$picto1, 34, 115, 13, 13, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
              // $pdf->Image('../pictograms/'.$picto1, 34, 224, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      }
      if (strlen($picto2)) {
      	$pdf->Image('../pictograms/'.$picto2, 48, 115, 13, 13, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
              // $pdf->Image('../pictograms/'.$picto2, 46, 224, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      }
      if (strlen($picto3)) {
      	$pdf->Image('../pictograms/'.$picto3, 62, 115, 13, 13, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
              // $pdf->Image('../pictograms/'.$picto3, 58, 224, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      }
      if (strlen($picto4)) {
      	$pdf->Image('../pictograms/'.$picto4, 76, 115, 13, 13, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
              // $pdf->Image('../pictograms/'.$picto4, 70, 224, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      }
      if (strlen($picto5)) {
      	$pdf->Image('../pictograms/'.$picto5, 90, 115, 13, 13, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
              // $pdf->Image('../pictograms/'.$picto5, 82, 224, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      }
      if (strlen($picto6)) {
      	$pdf->Image('../pictograms/'.$picto6, 104, 115, 13, 13, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
              // $pdf->Image('../pictograms/'.$picto6, 94, 224, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      }

      // LOGO Miele bas

      $pdf->Image($manager->getUrl('images/miele-logo.jpg'), 143, 28, 0, 8, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false); // Big
      $pdf->Image($manager->getUrl('images/miele-logo.jpg'), 128, 164, 0, 8, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false); // Small
      // Traits de coupe
      // 148x115 - Big
      $pdf->cropMark(31, 25, 10, 10, 'TL', array(0, 0, 0));     // Corner Haut Gauche Coordonnées droite haut
      $pdf->cropMark(179, 25, 10, 10, 'TR', array(0, 0, 0));    // Corner Haut Droit
      $pdf->cropMark(31, 140, 10, 10, 'BL', array(0, 0, 0));    // Corner Bas Gauche
      $pdf->cropMark(179, 140, 10, 10, 'BR', array(0, 0, 0));   // Corner Bas Droit
      // 133x85 - Small
      $pdf->cropMark(31, 160, 10, 10, 'TL', array(0, 0, 0));    // Corner Haut Gauche Coordonnées droite haut
      $pdf->cropMark(164, 160, 10, 10, 'TR', array(0, 0, 0));   // Corner Haut Droit
      $pdf->cropMark(31, 245, 10, 10, 'BL', array(0, 0, 0));    // Corner Bas Gauche
      $pdf->cropMark(164, 245, 10, 10, 'BR', array(0, 0, 0));   // Corner Bas Droit


      // ---------------------------------------------------------
      // Genere le PDF
      $pdf->Output('etiquette_' . $reftarif . '_miele.pdf', 'I');
      return new Response(); // To make the controller happy.
      }
    }

    public function getPathWeb(){
        $this->pathWeb = $this->get('kernel')->getRootDir() . '/../web/';
        return $this->pathWeb;
    }

    public function printAction($id, Request $request)
    {

      if ($request->isMethod('POST')) {
      $logo_miele = $this->getPathWeb() . 'images/miele-logo.jpg';

      $produit = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Produit')->find($id);
      // Creation du document PDF - Format origine A4
      $pdf = $this->get("white_october.tcpdf")->create('PDF_PAGE_ORIENTATION', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // Desactive tous graphismes dans le header et le footer - barres etc.
      $pdf->SetPrintHeader(false);
      $pdf->SetPrintFooter(false);

      // ---------------------------------------------------------
      // Récupérer les valeurs du formulaire
      $reftarif = $produit->getReftarif();
      $famille = $produit->getFamille()->getNom();
      $codesap = $produit->getCodesap();
      $codeean = $produit->getCodeean();
      $modepose = $produit->getModepose();
      $ecopart = $produit->getEcopart();
      $pvcm = $produit->getPvcm();
      $hauteurnet = $produit->getHauteurnet();
      $largeurnet = $produit->getLargeurnet();
      $profondeurnet = $produit->getProfondeurnet();
      $volpoidsnet = $produit->getValpoidsnet(); // 2017
      $pkgl = $produit->getPkgl(); // 2017
      $existe = $produit->getExiste();
      $arg1 = $produit->getInfo1();
      $arg2 = $produit->getInfo2();
      $arg3 = $produit->getInfo3();
      $arg4 = $produit->getInfo4();
      $arg5 = $produit->getInfo5();
      $photo = "";
      $prix = $request->request->get('prix');
      $prixTotal = floatval($prix) + floatval($ecopart);
      // New info for 2015 etiquettes
      $during = $produit->getDuring();
      $picto1 = "";
      $picto2 = "";
      $picto3 = "";
      $picto4 = "";
      $picto5 = "";
      $picto6 = "";
      // New info 2017
      $garantie = $produit->getGarantie();
      $member_id_stats = 0;

      // Récupérer la date
      $date = date("Ymd");

      // ---------------------------------------------------------
      // Police origine
      $pdf->SetFont('helvetica', '', '');

      // add a page
      $pdf->AddPage();

      // Information Taille
      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(10, 8, '5x4cm', 0, 'L', 0, 0, 50, 15, true, 0);
      $pdf->MultiCell(100, 8, '3.2x2.7cm', 0, 'L', 0, 0, 94, 19, true, 0);
      $pdf->MultiCell(10, 8, '6x4cm', 0, 'L', 0, 0, 145, 15, true, 0);
      $pdf->MultiCell(100, 8, 'Format 2015 134x84mm', 0, 'L', 0, 0, 85, 184, true, 0);

      // ***********************************************************
      // Creation logo Miele haut
      //$manager->getUrl('images/miele-logo.jpg')
      $pdf->Image($logo_miele, 47, 21, 0, 6, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      $pdf->Image($logo_miele, 142, 21, 0, 6, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);

      // Création et positionnement des Multicell - Grande etiquette
      $pdf->SetFont('helvetica', '', 13);
      $pdf->SetTextColor(134, 26, 34);
      $pdf->MultiCell(77, 6, ''.$famille, 0, 'L', 0, 0, 33, 77, true, 0);

      $pdf->SetFont('helvetica', 'B', 15);
      $pdf->SetTextColor(134, 26, 34);
      $pdf->MultiCell(77, 8, ''.$reftarif, 0, 'L', 0, 0, 33, 83, true, 0);



      $pdf->SetFont('helvetica', '', 9.5);
      $pdf->SetTextColor(0, 0, 0);
      if (strlen($reftarif) > 25) { // Selon longueur du titre
          $pdf->MultiCell(78, 8, $arg1, 0, 'L', 0, 0, 32, 98, true, 0);
      } else {
          $pdf->MultiCell(78, 8, $arg1, 0, 'L', 0, 0, 32, 92, true, 0);
      }

      $pdf->SetFont('helvetica', '', 9.5);
      $pdf->SetTextColor(0, 0, 0);
      if (strlen($reftarif) > 25) { // Selon longueur du titre
          $pdf->MultiCell(78, 8, $arg2, 0, 'L', 0, 0, 32, 106, true, 0);
      } else {
          $pdf->MultiCell(78, 8, $arg2, 0, 'L', 0, 0, 32, 100, true, 0);
      }

      $pdf->SetFont('helvetica', '', 9.5);
      $pdf->SetTextColor(0, 0, 0);
      if (strlen($reftarif) > 25) { // Selon longueur du titre
          $pdf->MultiCell(78, 8, $arg3, 0, 'L', 0, 0, 32, 114, true, 0);
      } else {
          $pdf->MultiCell(78, 8, $arg3, 0, 'L', 0, 0, 32, 108, true, 0);
      }

      $pdf->SetFont('helvetica', '', 9.5);
      $pdf->SetTextColor(0, 0, 0);
      if (strlen($reftarif) > 25) { // Selon longueur du titre
          $pdf->MultiCell(78, 8, $arg4, 0, 'L', 0, 0, 32, 122, true, 0);
      } else {
          $pdf->MultiCell(78, 8, $arg4, 0, 'L', 0, 0, 32, 116, true, 0);
      }

      $pdf->SetFont('helvetica', '', 9.5);
      $pdf->SetTextColor(0, 0, 0);
      if (strlen($reftarif) > 25) { // Selon longueur du titre
          $pdf->MultiCell(78, 8, $arg5, 0, 'L', 0, 0, 32, 130, true, 0);
      } else {
          $pdf->MultiCell(78, 8, $arg5, 0, 'L', 0, 0, 32, 124, true, 0);
      }

      // $pdf->SetFont('helvetica', '', 9.5);
      // $pdf->SetTextColor(0, 0, 0);
      // if (strlen($reftarif) > 25) { // Selon longueur du titre
      //     $pdf->MultiCell(78, 8, $during, 0, 'L', 0, 0, 32, 138, true, 0);
      // } else {
      //     $pdf->MultiCell(78, 8, $during, 0, 'L', 0, 0, 32, 132, true, 0);
      // }



      // $pdf->SetFont('helvetica', '', 9.5);
      // $pdf->SetTextColor(0, 0, 0);
      // $pdf->MultiCell(78, 8, $garantie, 0, 'L', 0, 0, 32, 140, true, 0);


      // VolPoidsNet 2017
      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(78, 4, 'Vol / Poids net : ' . $volpoidsnet , 0, 'L', 0, 0, 32, 143, true, 0);

      if ($famille != 'Accessoire') {
            // PKGL 2017
            $pdf->SetFont('helvetica', '', 7);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->MultiCell(78, 4, 'Prix Kg/L : ' . $pkgl , 0, 'L', 0, 0, 32, 147, true, 0);
      }

      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(78, 4, 'Dim. (H x L x P) : '.$hauteurnet.' x '.$largeurnet.' x '.$profondeurnet.' mm', 0, 'L', 0, 0, 32, 151, true, 0);

      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(78, 4, 'Code EAN : '.$codeean.'  Code SAP : '.$codesap, 0, 'L', 0, 0, 32, 155, true, 0);

      $pdf->SetFont('helvetica', '', 6.5);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(30, 4, '1000'.$date, 0, 'L', 0, 0, 32, 163, true, 0);

      $pdf->SetFont('helvetica', '', 34);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(70, 12, ''.$prix.' €', 0, 'R', 0, 0, 103, 146, true, 0);

      // Image produit
      if (strlen($photo)) {
              $pdf->Image('../photos_produits/'.$photo, 111, 83, 60, 60, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      }

      // Création rectangle rouge bas
      // if ($member_id_stats == 1 OR $member_id_stats == 87 OR $member_id_stats == 86 OR $member_id_stats == 102 OR $member_id_stats == 118) {
      //     $pdf->Rect(32, 167, 106, 8, 'F', '', array(0, 0, 0));
      // } else {
      //     $pdf->Rect(32, 167, 106, 8, 'F', '', array(134, 26, 34));
      // }

      // *************************************************************
      // Création et positionnement des Multicell - NEW etiquette 2015
      // FAMILLE
      $pdf->SetFont('helvetica', 'B', 10);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(90, 6, '' . $famille, 0, 'L', 0, 0, 35, 191, true, 0);

      // REF TARIF
      $pdf->SetFont('helvetica', '', 16);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(87, 8, '' . $reftarif, 0, 'L', 0, 0, 35, 196, true, 0);

      // ARGUMENTS
      $pdf->SetFont('helvetica', '', 11);
      $pdf->SetTextColor(0, 0, 0);
      if (!empty($arg1)) {
        if (strlen($reftarif) > 30) { // Selon longueur du titre
            $pdf->MultiCell(128, 6, '• '.$arg1, 0, 'L', 0, 0, 34, 213, true, 0);
        } else {
            $pdf->MultiCell(128, 6, '• '.$arg1, 0, 'L', 0, 0, 34, 207, true, 0);
        }
    }

    if (!empty($arg2)) {
      if (strlen($reftarif) > 30) { // Selon longueur du titre
          $pdf->MultiCell(128, 6, '• '.$arg2, 0, 'L', 0, 0, 34, 220, true, 0);
      } else {
          $pdf->MultiCell(128, 6, '• '.$arg2, 0, 'L', 0, 0, 34, 214, true, 0);
      }
    }

    if (!empty($arg3)) {
      if (strlen($reftarif) > 30) { // Selon longueur du titre
          $pdf->MultiCell(128, 6, '• '.$arg3, 0, 'L', 0, 0, 34, 227, true, 0);
      } else {
          $pdf->MultiCell(128, 6, '• '.$arg3, 0, 'L', 0, 0, 34, 221, true, 0);
      }
    }

    if (!empty($arg4)) {
      if (strlen($reftarif) > 30) { // Selon longueur du titre
          $pdf->MultiCell(128, 6, '• '.$arg4, 0, 'L', 0, 0, 34, 234, true, 0);
      } else {
          $pdf->MultiCell(128, 6, '• '.$arg4, 0, 'L', 0, 0, 34, 228, true, 0);
      }
    }

    if (!empty($arg5)) {
      if (strlen($reftarif) > 30) { // Selon longueur du titre
          $pdf->MultiCell(128, 6, '• '.$arg5, 0, 'L', 0, 0, 34, 241, true, 0);
      } else {
          $pdf->MultiCell(128, 6, '• '.$arg5, 0, 'L', 0, 0, 34, 235, true, 0);
      }
    }

    // During
    $pdf->SetFont('helvetica', '', 7);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->MultiCell(86, 4, $during, 0, 'L', 0, 0, 34, 249, true, 0);

    // Garantie
    $pdf->SetFont('helvetica', '', 7);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->MultiCell(130, 4, $garantie, 0, 'L', 0, 0, 34, 252, true, 0);

    // Existe
      $pdf->SetFont('helvetica', '', 8);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(86, 4, $existe, 0, 'L', 0, 0, 34, 253, true, 0);

      // Code EAN
      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(78, 4, 'Code EAN : ' . $codeean , 0, 'L', 0, 0, 34, 260, true, 0);

      // if ($famille != 'Accessoire') {
      //       // PKGL
      //       $pdf->SetFont('helvetica', '', 7);
      //       $pdf->SetTextColor(0, 0, 0);
      //       $pdf->MultiCell(78, 4, 'Prix Kg/L : ' . $pkgl , 0, 'L', 0, 0, 34, 263, true, 0);
      // }
      if ($famille != 'Accessoire') {
            // CODE SAP
            $pdf->SetFont('helvetica', '', 7);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->MultiCell(78, 4, 'Code SAP : ' . $codesap , 0, 'L', 0, 0, 34, 263, true, 0);
      }

      // TAILLE
      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(78, 4, 'Dim. (H x L x P) : ' . $hauteurnet . ' x ' . $largeurnet . ' x ' . $profondeurnet . ' mm', 0, 'L', 0, 0, 34, 266, true, 0);

      // POIDS NET
      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(88, 4, 'Poids net : ' . $volpoidsnet, 0, 'L', 0, 0, 34, 269, true, 0);

      // Price
      // $pdf->SetFont('helvetica', '', 24);
      // $pdf->SetTextColor(0, 0, 0);
      // $pdf->MultiCell(55, 7, '' . $prix . ' €', 0, 'R', 0, 0, 105, 265, true, 0); // Small

      // Price
      $pdf->SetFont('helvetica', '', 17);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(35, 7, '' . $prix . ' €', 0, 'R', 0, 0, 112, 158, true, 0); // Big
      $pdf->SetFont('helvetica', '', 16);
      // $pdf->MultiCell(35, 7, '' . $prix . ' €', 0, 'R', 0, 0, 99, 227, true, 0); // Small
      $pdf->MultiCell(35, 7, '' . $prix . ' €', 0, 'R', 0, 0, 99, 262, true, 0); // 2017

      $pdf->SetFont('helvetica', '', 10);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(38, 4, '+ ' . $ecopart . ' € d\'éco-part.' , 0, 'L', 0, 0, 147, 159, true, 0); // Big
      $pdf->SetFont('helvetica', '', 9);
      // $pdf->MultiCell(35, 4, '+ ' . $ecopart . ' € d\'éco-part.' , 0, 'L', 0, 0, 134, 228, true, 0); // Small
      $pdf->MultiCell(35, 4, '+ ' . $ecopart . ' € d\'éco-part.' , 0, 'L', 0, 0, 134, 263, true, 0); // 2017

      $pdf->SetFont('helvetica', '', 17);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(60, 4, 'soit ' . $prixTotal . ' €' , 0, 'R', 0, 0, 116, 165, true, 0); // Big
      $pdf->SetFont('helvetica', '', 16);
      // $pdf->MultiCell(40, 4, 'soit ' . $prixTotal . ' €' , 0, 'R', 0, 0, 120, 233, true, 0); // Small
      $pdf->MultiCell(40, 4, 'soit ' . $prixTotal . ' €' , 0, 'R', 0, 0, 120, 266, true, 0); // 2017

      // RED LINE
      $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
      if (strlen($reftarif) > 30) { // Selon longueur du titre
          $pdf->Line(32, 211, 161, 211, $style); // Creation line haute
          $pdf->Line(32, 259, 161, 259, $style);
      } else {
          $pdf->Line(32, 205, 161, 205, $style); // Creation line haute
          $pdf->Line(32, 260, 161, 260, $style);
      }

      /* Désactivation des pictos car ils ne sont pas utilisés - BY CM le 11/10/2015 */
      // Pictogrammes
      //if (strlen($picto1)) {
      //	$pdf->Image('../pictograms/'.$picto1, 33, 252, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      //}
      //if (strlen($picto2)) {
      //	$pdf->Image('../pictograms/'.$picto2, 45, 252, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      //}
      //if (strlen($picto3)) {
      //	$pdf->Image('../pictograms/'.$picto3, 57, 252, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      //}
      //if (strlen($picto4)) {
      //	$pdf->Image('../pictograms/'.$picto4, 69, 252, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      //}
      //if (strlen($picto5)) {
      //	$pdf->Image('../pictograms/'.$picto5, 81, 252, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      //}
      //if (strlen($picto6)) {
      //	$pdf->Image('../pictograms/'.$picto6, 93, 252, 11, 11, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      //}

      // LOGO Miele bas
        $pdf->Image($logo_miele, 88, 188, 0, 8, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);

      // ***********************************************************
      // Création et positionnement des Multicell - Medium etiquette
      $pdf->SetFont('helvetica', 'B', 11);
      if ($member_id_stats == 1 OR $member_id_stats == 87 OR $member_id_stats == 86 OR $member_id_stats == 102 OR $member_id_stats == 118) {
          $pdf->SetTextColor(0, 0, 0);
      } else {
          $pdf->SetTextColor(134, 26, 34);
      }

      // Block right
      $pdf->MultiCell(54, 30, ''.$reftarif, 0, 'C', 0, 0, 123, 27, true, 0);
      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      if ($famille != 'Accessoire') {
            $pdf->MultiCell(78, 4, 'Prix Kg/L : ' . $pkgl, 0, 'L', 0, 0, 123, 46, true, 0); // 2017
      }
      $pdf->MultiCell(78, 4, 'Code SAP : ' . $codesap, 0, 'L', 0, 0, 123, 49, true, 0);
      $pdf->SetFont('helvetica', '', 12);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(25, 5, ''.$prix.' €', 0, 'R', 0, 0, 154, 47.6, true, 0);

      // **********************************************************
      // Création et positionnement des Multicell - Small etiquette
      $pdf->SetFont('helvetica', 'B', 11);
      if ($member_id_stats == 1 OR $member_id_stats == 87 OR $member_id_stats == 86 OR $member_id_stats == 102 OR $member_id_stats == 118) {
          $pdf->SetTextColor(0, 0, 0);
      } else {
          $pdf->SetTextColor(134, 26, 34);
      }

      // Left block
      $pdf->MultiCell(48, 27, ''.$reftarif, 0, 'C', 0, 0, 31, 27, true, 0);
      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      if ($famille != 'Accessoire') {
            $pdf->MultiCell(78, 4, 'Vol / Poids net : ' . $volpoidsnet , 0, 'L', 0, 0, 31, 48, true, 0);
            $pdf->MultiCell(78, 4, 'Prix Kg/L : ' . $pkgl, 0, 'L', 0, 0, 31, 51, true, 0); // 2017
      }
      $pdf->MultiCell(78, 4, 'Code SAP : ' . $codesap, 0, 'L', 0, 0, 31, 55, true, 0);

      $pdf->SetFont('helvetica', '', 13);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(25, 5, $prix.' €', 0, 'R', 0, 0, 53, 53, true, 0);
      $pdf->MultiCell(25, 5, $prix.' €', 0, 'R', 0, 0, 53, 53, true, 0);

      // Création rectangle rouge bas
      // if ($member_id_stats == 1 OR $member_id_stats == 87 OR $member_id_stats == 86 OR $member_id_stats == 102 OR $member_id_stats == 118) {
      //     $pdf->Rect(31, 54, 32, 3, 'F', '', array(0, 0, 0));
      // } else {
      //     $pdf->Rect(31, 54, 32, 3, 'F', '', array(134, 26, 34));
      // }

      // Creation logo Miele bas
      // if ($member_id_stats == 1 OR $member_id_stats == 87 OR $member_id_stats == 86 OR $member_id_stats == 102 OR $member_id_stats == 118) {
      //     $pdf->Image($logo_miele, 65, 54, 0, 3, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      // } else {
      //     $pdf->Image($logo_miele, 65, 54, 0, 3, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      // }

      // **********************************************************
      // Création et positionnement des Multicell - Very Small etiquette
      $pdf->SetFont('helvetica', 'B', 7);
      if ($member_id_stats == 1 OR $member_id_stats == 87 OR $member_id_stats == 86 OR $member_id_stats == 102 OR $member_id_stats == 118) {
          $pdf->SetTextColor(0, 0, 0);
      } else {
          $pdf->SetTextColor(134, 26, 34);
      }

      // Center block
      $pdf->MultiCell(27, 19, ''.$reftarif, 0, 'C', 0, 0, 87, 23, true, 0);
      $pdf->SetFont('helvetica', '', 7);
      $pdf->SetTextColor(0, 0, 0);
      //$pdf->MultiCell(27, 4, $pkgl, 1, 'C', 0, 0, 87, 38, true, 0); // 2017
      if ($famille != 'Accessoire') {
            $pdf->MultiCell(27, 4, 'Vol / Poids net : ' . $volpoidsnet, 0, 'C', 0, 0, 87, 33, true, 0);
            $pdf->MultiCell(24, 4, 'Prix Kg/L : ' . $pkgl, 0, 'C', 0, 0, 91, 37, true, 0); // 2017
      }
      $pdf->MultiCell(27, 4, 'Code SAP : '.$codesap, 0, 'C', 0, 0, 87, 44, true, 0);

      $pdf->SetFont('helvetica', '', 11);
      $pdf->SetTextColor(0, 0, 0);
      $pdf->MultiCell(27, 5, ''.$prix.' €', 0, 'C', 0, 0, 87, 47, true, 0);


      // ***********************************
      // Image support etiquettes
      $pdf->Image($this->getPathWeb() . 'images/mw_etiquette_chevalet.jpg', 50, 60, 10, 10, 'JPEG', '', '', true, 150, '', false, false, 0, false, false, false);
      $pdf->Image($this->getPathWeb() . 'images/mw_etiquette_broche2.jpg', 95, 60, 10, 20, 'JPEG', '', '', true, 150, '', false, false, 0, false, false, false);
      $pdf->Image($this->getPathWeb() . 'images/mw_etiquette_3265.jpg', 147, 60, 0, 8, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);

      // ***********************************
      // Traits de coupe  - Grande etiquette
      $pdf->cropMark(30, 72, 10, 10, 'TL', array(0,0,0));
      $pdf->cropMark(178, 72, 10, 10, 'TR', array(0,0,0));
      $pdf->cropMark(30, 178, 10, 10, 'BL', array(0,0,0));
      $pdf->cropMark(178, 178, 10, 10, 'BR', array(0,0,0));

      // 133x85 - Small - NEW ETIQUETTE 2015
      $pdf->cropMark(30, 188, 10, 10, 'TL', array(0, 0, 0));      // Corner Haut Gauche Coordonnées droite haut
      $pdf->cropMark(163, 188, 10, 10, 'TR', array(0, 0, 0));     // Corner Haut Droit
      $pdf->cropMark(30, 273, 10, 10, 'BL', array(0, 0, 0));      // Corner Bas Gauche
      $pdf->cropMark(163, 273, 10, 10, 'BR', array(0, 0, 0));     // Corner Bas Droit

      // Traits de coupe  - Small etiquette
      $pdf->cropMark(30, 20, 10, 10, 'TL', array(0,0,0));         // Corner Haut Gauche Coordonnées droite haut
      $pdf->cropMark(79, 20, 10, 10, 'TR', array(0,0,0));         // Corner Haut Droit
      $pdf->cropMark(30, 59, 10, 10, 'BL', array(0,0,0));         // Corner Bas Gauche
      $pdf->cropMark(79, 59, 10, 10, 'BR', array(0,0,0));         // Corner Bas Droit

      // Traits de coupe  - Medium etiquette
      $pdf->cropMark(122, 20, 10, 10, 'TL', array(0,0,0));
      $pdf->cropMark(180, 20, 10, 10, 'TR', array(0,0,0));
      $pdf->cropMark(122, 59, 10, 10, 'BL', array(0,0,0));
      $pdf->cropMark(180, 59, 10, 10, 'BR', array(0,0,0));

      // Traits de coupe - VERY SMALL etiquette
      $pdf->cropMark(87, 20, 5, 5, 'TL', array(0,0,0));
      $pdf->cropMark(114, 20, 5, 5, 'TR', array(0,0,0));
      $pdf->cropMark(87, 52, 5, 5, 'BL', array(0,0,0));
      $pdf->cropMark(114, 52, 5, 5, 'BR', array(0,0,0));

      // ---------------------------------------------------------

      // Genere le PDF
      $pdf->Output('etiquette_'.$reftarif.'_miele.pdf', 'I');
      return new Response(); // To make the controller happy.
      }
    }

    public function addAction(Request $request){

      if ($request->isMethod('POST')) {
        $produit = new Produit();
        $produit->setCreated(new \Datetime());
        $produit->setUpdated(new \Datetime());
        $produit->setReftarif($request->request->get('reftarif'));
        $produit->setReftarifsansespace($request->request->get('reftarifsansespace'));
        $produit->setCodeean($request->request->get('codeean'));
        $produit->setCodesap($request->request->get('codesap'));
        $produit->setModepose($request->request->get('modepose'));
        $produit->setEcopart($request->request->get('ecopart'));
        $produit->setPvcm($request->request->get('pvcm'));
        $produit->setPkgl($request->request->get('pkgl'));
        $produit->setHauteurnet($request->request->get('hauteurnet'));
        $produit->setLargeurnet($request->request->get('largeurnet'));
        $produit->setProfondeurnet($request->request->get('profondeurnet'));
        $produit->setExiste($request->request->get('existe'));
        $produit->setInfo1($request->request->get('info1'));
        $produit->setInfo2($request->request->get('info2'));
        $produit->setInfo3($request->request->get('info3'));
        $produit->setInfo4($request->request->get('info4'));
        $produit->setInfo5($request->request->get('info5'));
        $produit->setDuring($request->request->get('during'));
        $produit->setGarantie($request->request->get('garantie'));
        $produit->setValpoidsnet($request->request->get('valpoidsnet'));
        $produit->setGamme(intval($request->request->get('gamme')));

        $famille = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Famille')->find($request->request->get('famille'));
        $produit->setFamille($famille);

        $em = $this->getDoctrine()->getManager();
        $em->persist($produit);
        $em->flush();

        $request->getSession()->getFlashBag()->add('notice', 'Le produit a bien été enregistré.');

        // Puis on redirige vers la page de visualisation de cettte annonce
        return $this->redirectToRoute('etiquette_view', array('id' => $produit->getId()));
      }

      $familles = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Famille')->findAll();
      return $this->render('@Etiquette/Etiquette/add.html.twig', array('familles' => $familles));
    }

    public function editAction($id, Request $request){

      if ($request->isMethod('POST')) {
        $produit = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Produit')->find($id);
        $produit->setUpdated(new \Datetime());
        $produit->setReftarif($request->request->get('reftarif'));
        $produit->setReftarifsansespace($request->request->get('reftarifsansespace'));
        $produit->setCodeean($request->request->get('codeean'));
        $produit->setCodesap($request->request->get('codesap'));
        $produit->setModepose($request->request->get('modepose'));
        $produit->setEcopart($request->request->get('ecopart'));
        $produit->setPvcm($request->request->get('pvcm'));
        $produit->setPkgl($request->request->get('pkgl'));
        $produit->setHauteurnet($request->request->get('hauteurnet'));
        $produit->setLargeurnet($request->request->get('largeurnet'));
        $produit->setProfondeurnet($request->request->get('profondeurnet'));
        $produit->setExiste($request->request->get('existe'));
        $produit->setInfo1($request->request->get('info1'));
        $produit->setInfo2($request->request->get('info2'));
        $produit->setInfo3($request->request->get('info3'));
        $produit->setInfo4($request->request->get('info4'));
        $produit->setInfo5($request->request->get('info5'));
        $produit->setDuring($request->request->get('during'));
        $produit->setGarantie($request->request->get('garantie'));
        $produit->setValpoidsnet($request->request->get('valpoidsnet'));
        $produit->setGamme(intval($request->request->get('gamme')));

        $famille = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Famille')->find($request->request->get('famille'));
        $produit->setFamille($famille);

        $em = $this->getDoctrine()->getManager();
        $em->persist($produit);
        $em->flush();
        $request->getSession()->getFlashBag()->add('notice', 'Le produit a bien été modifié.');

        // Puis on redirige vers la page de visualisation de cettte annonce
        return $this->redirectToRoute('etiquette_view', array('id' => $id));
      }

      $produit = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Produit')->find($id);
      $familles = $this->getDoctrine()->getManager()->getRepository('EtiquetteBundle:Famille')->findAll();
      return $this->render('@Etiquette/Etiquette/edit.html.twig', array('produit' => $produit, 'familles' => $familles));
    }
}
