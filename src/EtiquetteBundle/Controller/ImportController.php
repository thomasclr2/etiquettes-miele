<?php

namespace EtiquetteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use EtiquetteBundle\Entity\Produit;
use EtiquetteBundle\Entity\Famille;

class ImportController extends Controller
{
    public function productAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	// Create simple form
		$form = $this->createFormBuilder()
		    ->add('file', FileType::class, array(
		    	'attr' => array(
		    		'class' => 'form-control'
		    	),
		    	'label' => 'Ficher (CSV)'
		    ))
		    ->add('submit', SubmitType::class, array(
		    	'label' => 'Envoyer',
		    	'attr' => array(
		    		'class' => 'btn btn-primary'
		    	)
		    ))
		    ->getForm()
		;

		$form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

        	// Get data in CSV
			$serializer = new Serializer(array(new ObjectNormalizer()), array(new CsvEncoder(',', '"', '\\', '|')));
			$datas = $serializer->decode(file_get_contents($form->getData()['file']->getPathName()), 'csv');

			$products = $em->getRepository(Produit::class)->findAll();
			foreach ($products as $product) {
				$em->remove($product);
			}

			$families = $em->getRepository(Famille::class)->findAll();
			foreach ($families as $family) {
				$em->remove($family);
			}

			$em->flush();
            $nbAdd = 0;

			foreach ($datas as $data) {
				$product = new Produit();
				$family = $em->getRepository(Famille::class)->findBy(['nom' => $data['FAMILLE']]);

				if (count($family) > 0) {
					$product->setFamille($family[0]);
				} else {
					$family = new Famille();
					$family->setNom($data['FAMILLE']);
					$em->persist($family);
					$em->flush();

					$product->setFamille($family);
				}

				foreach ($families as $family) {
					if (strtolower($family->getNom()) === strtolower($data['FAMILLE'])) {
						$product->setFamille($family);
					}
				}

				$product->setGamme($data['gamme']);
				$product->setReftarif($data['REF TARIF']);
				$product->setReftarifsansespace($data['Ref_tarif_sans_espace']);
				$product->setCodesap($data['CODE SAP']);
				$product->setCodeean($data['CODE EAN']);
				$product->setModepose($data['MODE POSE']);
				$product->setEcopart($data['Eco Part']);
				$product->setPvcm($data['Prix catalogue']);
				$product->setPkgl($data['Prix kg/L']);
				$product->setHauteurnet($data['HAUTEUR NETTE (mm)']);
				$product->setLargeurnet($data['LARGEUR NETTE (mm)']);
				$product->setProfondeurnet($data['PROFONDEUR NETTE (mm)']);
				$product->setExiste($data['Existe']);
				$product->setInfo1($data['Argument Etiquette 1']);
				$product->setInfo2($data['Argument Etiquette 2']);
				$product->setInfo3($data['Argument Etiquette 3']);
				$product->setInfo4($data['Argument Etiquette 4']);
				$product->setInfo5($data['Argument Etiquette 5']);
				$product->setDuring($data['durée']);
				$product->setGarantie($data['durée bis']);
				$product->setValpoidsnet($data['Vol./Poids net']);

				$em->persist($product);
                $nbAdd ++;
			}

			$em->flush();

			$this->addFlash('notice', 'Votre CSV à été importé ! ' . $nbAdd . ' produits');

			return $this->redirectToRoute('import_product');
		}
    	
    	return $this->render('@Etiquette/Import/produit.html.twig', array(
    		'form' => $form->createView(),
    	));
    }

    public function imagesAction(Request $request)
    {	
    	// Create simple form
		$form = $this->createFormBuilder()
		    ->add('files', FileType::class, array(
		    	'attr' => array(
		    		'class' => 'form-control'
		    	),
		    	'label' => 'Images (.jpg)',
		    	'multiple' => true
		    ))
		    ->add('submit', SubmitType::class, array(
		    	'label' => 'Envoyer',
		    	'attr' => array(
		    		'class' => 'btn btn-primary'
		    	)
		    ))
		    ->getForm()
		;

		$form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

        	foreach($form->getData()['files'] as $file) {
		        $file->move('uploads/produits', $file->getClientOriginalName());
		        unset($file);
		    }

		    $this->addFlash('notice', 'Vos images ont été importés !');

		    return $this->redirectToRoute('import_images');
		}
    	
    	return $this->render('@Etiquette/Import/images.html.twig', array(
    		'form' => $form->createView(),
    	));
    }
}