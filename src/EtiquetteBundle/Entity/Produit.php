<?php

namespace EtiquetteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="EtiquetteBundle\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="reftarif", type="string", length=255)
     */
    private $reftarif;

    /**
     * @var string
     *
     * @ORM\Column(name="reftarifsansespace", type="string", length=255)
     */
    private $reftarifsansespace;

    /**
     * @var string
     *
     * @ORM\Column(name="codesap", type="string", length=30)
     */
    private $codesap;

    /**
     * @var string
     *
     * @ORM\Column(name="codeean", type="string", length=15)
     */
    private $codeean;

    /**
     * @var string
     *
     * @ORM\Column(name="modepose", type="string", length=6, nullable=true)
     */
    private $modepose;

    /**
     * @var string
     *
     * @ORM\Column(name="ecopart", type="string", length=10, nullable=true)
     */
    private $ecopart;

    /**
     * @var string
     *
     * @ORM\Column(name="pvcm", type="string", length=10, nullable=true)
     */
    private $pvcm;

    /**
     * @var string
     *
     * @ORM\Column(name="pkgl", type="string", length=10, nullable=true)
     */
    private $pkgl;

    /**
     * @var string
     *
     * @ORM\Column(name="hauteurnet", type="string", length=45, nullable=true)
     */
    private $hauteurnet;

    /**
     * @var string
     *
     * @ORM\Column(name="largeurnet", type="string", length=45, nullable=true)
     */
    private $largeurnet;

    /**
     * @var string
     *
     * @ORM\Column(name="profondeurnet", type="string", length=45, nullable=true)
     */
    private $profondeurnet;

    /**
     * @var string
     *
     * @ORM\Column(name="existe", type="string", length=255, nullable=true)
     */
    private $existe;

    /**
     * @var string
     *
     * @ORM\Column(name="info1", type="string", length=255, nullable=true)
     */
    private $info1;

    /**
     * @var string
     *
     * @ORM\Column(name="info2", type="string", length=255, nullable=true)
     */
    private $info2;

    /**
     * @var string
     *
     * @ORM\Column(name="info3", type="string", length=255, nullable=true)
     */
    private $info3;

    /**
     * @var string
     *
     * @ORM\Column(name="info4", type="string", length=255, nullable=true)
     */
    private $info4;

    /**
     * @var string
     *
     * @ORM\Column(name="info5", type="string", length=255, nullable=true)
     */
    private $info5;

    /**
     * @var string
     *
     * @ORM\Column(name="during", type="string", length=255, nullable=true)
     */
    private $during;

    /**
     * @var string
     *
     * @ORM\Column(name="garantie", type="string", length=255, nullable=true)
     */
    private $garantie;

    /**
     * @var string
     *
     * @ORM\Column(name="valpoidsnet", type="string", length=10, nullable=true)
     */
    private $valpoidsnet;

    /**
     * @var int
     *
     * @ORM\Column(name="gamme", type="integer", nullable=true)
     */
    private $gamme;

    /**
     * @ORM\ManyToOne(targetEntity="EtiquetteBundle\Entity\Famille", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $famille;

    public function __construct() {
        $this->created = new \Datetime('now');
        $this->updated = new \Datetime('now');
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Produit
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Produit
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set reftarif.
     *
     * @param string $reftarif
     *
     * @return Produit
     */
    public function setReftarif($reftarif)
    {
        $this->reftarif = $reftarif;

        return $this;
    }

    /**
     * Get reftarif.
     *
     * @return string
     */
    public function getReftarif()
    {
        return $this->reftarif;
    }

    /**
     * Set reftarifsansespace.
     *
     * @param string $reftarifsansespace
     *
     * @return Produit
     */
    public function setReftarifsansespace($reftarifsansespace)
    {
        $this->reftarifsansespace = $reftarifsansespace;

        return $this;
    }

    /**
     * Get reftarifsansespace.
     *
     * @return string
     */
    public function getReftarifsansespace()
    {
        return $this->reftarifsansespace;
    }

    /**
     * Set codesap.
     *
     * @param string $codesap
     *
     * @return Produit
     */
    public function setCodesap($codesap)
    {
        $this->codesap = $codesap;

        return $this;
    }

    /**
     * Get codesap.
     *
     * @return string
     */
    public function getCodesap()
    {
        return $this->codesap;
    }

    /**
     * Set codeean.
     *
     * @param string $codeean
     *
     * @return Produit
     */
    public function setCodeean($codeean)
    {
        $this->codeean = $codeean;

        return $this;
    }

    /**
     * Get codeean.
     *
     * @return string
     */
    public function getCodeean()
    {
        return $this->codeean;
    }

    /**
     * Set modepose.
     *
     * @param string $modepose
     *
     * @return Produit
     */
    public function setModepose($modepose)
    {
        $this->modepose = $modepose;

        return $this;
    }

    /**
     * Get modepose.
     *
     * @return string
     */
    public function getModepose()
    {
        return $this->modepose;
    }

    /**
     * Set ecopart.
     *
     * @param string $ecopart
     *
     * @return Produit
     */
    public function setEcopart($ecopart)
    {
        $this->ecopart = $ecopart;

        return $this;
    }

    /**
     * Get ecopart.
     *
     * @return string
     */
    public function getEcopart()
    {
        return $this->ecopart;
    }

    /**
     * Set pvcm.
     *
     * @param string $pvcm
     *
     * @return Produit
     */
    public function setPvcm($pvcm)
    {
        $this->pvcm = $pvcm;

        return $this;
    }

    /**
     * Get pvcm.
     *
     * @return string
     */
    public function getPvcm()
    {
        return $this->pvcm;
    }

    /**
     * Set pkgl.
     *
     * @param string $pkgl
     *
     * @return Produit
     */
    public function setPkgl($pkgl)
    {
        $this->pkgl = $pkgl;

        return $this;
    }

    /**
     * Get pkgl.
     *
     * @return string
     */
    public function getPkgl()
    {
        return $this->pkgl;
    }

    /**
     * Set hauteurnet.
     *
     * @param string $hauteurnet
     *
     * @return Produit
     */
    public function setHauteurnet($hauteurnet)
    {
        $this->hauteurnet = $hauteurnet;

        return $this;
    }

    /**
     * Get hauteurnet.
     *
     * @return string
     */
    public function getHauteurnet()
    {
        return $this->hauteurnet;
    }

    /**
     * Set largeurnet.
     *
     * @param string $largeurnet
     *
     * @return Produit
     */
    public function setLargeurnet($largeurnet)
    {
        $this->largeurnet = $largeurnet;

        return $this;
    }

    /**
     * Get largeurnet.
     *
     * @return string
     */
    public function getLargeurnet()
    {
        return $this->largeurnet;
    }

    /**
     * Set profondeurnet.
     *
     * @param string $profondeurnet
     *
     * @return Produit
     */
    public function setProfondeurnet($profondeurnet)
    {
        $this->profondeurnet = $profondeurnet;

        return $this;
    }

    /**
     * Get profondeurnet.
     *
     * @return string
     */
    public function getProfondeurnet()
    {
        return $this->profondeurnet;
    }

    /**
     * Set existe.
     *
     * @param string $existe
     *
     * @return Produit
     */
    public function setExiste($existe)
    {
        $this->existe = $existe;

        return $this;
    }

    /**
     * Get existe.
     *
     * @return string
     */
    public function getExiste()
    {
        return $this->existe;
    }

    /**
     * Set info1.
     *
     * @param string $info1
     *
     * @return Produit
     */
    public function setInfo1($info1)
    {
        $this->info1 = $info1;

        return $this;
    }

    /**
     * Get info1.
     *
     * @return string
     */
    public function getInfo1()
    {
        return $this->info1;
    }

    /**
     * Set info2.
     *
     * @param string $info2
     *
     * @return Produit
     */
    public function setInfo2($info2)
    {
        $this->info2 = $info2;

        return $this;
    }

    /**
     * Get info2.
     *
     * @return string
     */
    public function getInfo2()
    {
        return $this->info2;
    }

    /**
     * Set info3.
     *
     * @param string $info3
     *
     * @return Produit
     */
    public function setInfo3($info3)
    {
        $this->info3 = $info3;

        return $this;
    }

    /**
     * Get info3.
     *
     * @return string
     */
    public function getInfo3()
    {
        return $this->info3;
    }

    /**
     * Set info4.
     *
     * @param string $info4
     *
     * @return Produit
     */
    public function setInfo4($info4)
    {
        $this->info4 = $info4;

        return $this;
    }

    /**
     * Get info4.
     *
     * @return string
     */
    public function getInfo4()
    {
        return $this->info4;
    }

    /**
     * Set info5.
     *
     * @param string $info5
     *
     * @return Produit
     */
    public function setInfo5($info5)
    {
        $this->info5 = $info5;

        return $this;
    }

    /**
     * Get info5.
     *
     * @return string
     */
    public function getInfo5()
    {
        return $this->info5;
    }

    /**
     * Set during.
     *
     * @param string $during
     *
     * @return Produit
     */
    public function setDuring($during)
    {
        $this->during = $during;

        return $this;
    }

    /**
     * Get during.
     *
     * @return string
     */
    public function getDuring()
    {
        return $this->during;
    }

    /**
     * Set garantie.
     *
     * @param string $garantie
     *
     * @return Produit
     */
    public function setGarantie($garantie)
    {
        $this->garantie = $garantie;

        return $this;
    }

    /**
     * Get garantie.
     *
     * @return string
     */
    public function getGarantie()
    {
        return $this->garantie;
    }

    /**
     * Set valpoidsnet.
     *
     * @param string $valpoidsnet
     *
     * @return Produit
     */
    public function setValpoidsnet($valpoidsnet)
    {
        $this->valpoidsnet = $valpoidsnet;

        return $this;
    }

    /**
     * Get valpoidsnet.
     *
     * @return string
     */
    public function getValpoidsnet()
    {
        return $this->valpoidsnet;
    }


    /**
     * Set gamme.
     *
     * @param int $gamme
     *
     * @return Produit
     */
    public function setGamme($gamme)
    {
        $this->gamme = $gamme;

        return $this;
    }

    /**
     * Get gamme.
     *
     * @return int
     */
    public function getGamme()
    {
        return $this->gamme;
    }

    /**
     * Set famille.
     *
     * @param \EtiquetteBundle\Entity\Famille|null $famille
     *
     * @return Produit
     */
    public function setFamille(\EtiquetteBundle\Entity\Famille $famille = null)
    {
        $this->famille = $famille;

        return $this;
    }

    /**
     * Get famille.
     *
     * @return \EtiquetteBundle\Entity\Famille|null
     */
    public function getFamille()
    {
        return $this->famille;
    }
}
